﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task12PostGrad.Models
{
    public class StudentProject
    {
        //Primary Key

        public int StudenId { get; set; }
        public int ProjectId { get; set; }

        //Navigation

        public Student Student { get; set; }
        public Project Project { get; set; }

    }
}
