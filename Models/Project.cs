﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task12PostGrad.Models
{
    public class Project
    {

        public int ProjectId { get; set; }
        public string Subject { get; set; }

        //Forreign key
        public int ProfessorId { get; set; }

        //Nav
        public Professor Professor { get; set; }


        // One to many
        public ICollection<StudentProject> StudentProjects { get; set; }

    }
}
