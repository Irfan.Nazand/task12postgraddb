﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task12PostGrad.Models
{
    public class Professor
    {
        //Primary Key
        public int Id { get; set; }

        //Fields
        public string FirstName { get; set; }
        public string LastName { get; set; }

        //Relationships


        // A professor can have many students
        public ICollection<Student> Students { get; set; }
        public ICollection<Project> Projects { get; set; }
    }
}
