﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Task12PostGrad.Models
{
    public class Student
    {
        // Primary Key
        public int Id { get; set; }
        
        // Fields
        public string FirstName { get; set; }
        public string LastName { get; set; }

        // Relationships
        // Forreign keys
        public int ProfessorId { get; set; }
        
        //Navigation property
        public Professor Professor { get; set; }

        // A student can be enrolled in many classes
        public ICollection<StudentProject> StudentProjects { get; set; }
    }
}
