﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using System;
using System.Collections.Generic;
using System.Text;

namespace Task12PostGrad.Models
{
    public class PostGradDbContext : DbContext 
    {
        // Using tables
        public DbSet<Professor> Professors { get; set; }
        public DbSet<Student> Students { get; set; }
        public DbSet<StudentProject> StudentProjects { get; set; }
        public DbSet<Project> Projects { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Data Source= PC7269\\SQLEXPRESS01; Initial Catalog=PostGradDb; Integrated Security=True;");
        }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<StudentProject>().HasKey(pr => new { pr.StudenId, pr.ProjectId });

            modelBuilder.Entity<Project>().HasData(new Project { ProjectId = 1, Subject = "Java", ProfessorId = 1 });
            modelBuilder.Entity<Project>().HasData(new Project { ProjectId = 2, Subject = ".NET", ProfessorId = 1 });

            modelBuilder.Entity<Student>().HasData(new Student() { Id = 1, FirstName = "Irfan", LastName = "Nazand", ProfessorId = 1 }); ;
            modelBuilder.Entity<Student>().HasData(new Student() { Id = 2, FirstName = "Sven", LastName = "Richard", ProfessorId = 1 }); ;


            modelBuilder.Entity<Professor>().HasData(new Professor() { Id = 1, FirstName = "Nick", LastName = "Lennox", Students = { }, Projects = { } });
            modelBuilder.Entity<Professor>().HasData(new Professor() { Id = 2, FirstName = "Nickyboi", LastName = "Lennnox", Students = { }, Projects = { } });
            modelBuilder.Entity<Professor>().HasData(new Professor() { Id = 3, FirstName = "Dewald", LastName = "IdontKnow", Students = { }, Projects = { } });
            modelBuilder.Entity<Professor>().HasData(new Professor() { Id = 4, FirstName = "Irfa", LastName = "Naza", Students = { }, Projects = { } });
            modelBuilder.Entity<Professor>().HasData(new Professor() { Id = 5, FirstName = "Irfannn", LastName = "Naazaaaand", Students = { }, Projects = { } });

        } 
    }
}
