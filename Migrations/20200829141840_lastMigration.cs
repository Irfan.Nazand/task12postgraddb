﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Task12PostGrad.Migrations
{
    public partial class lastMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Professors",
                columns: new[] { "Id", "FirstName", "LastName" },
                values: new object[,]
                {
                    { 2, "Nickyboi", "Lennnox" },
                    { 3, "Dewald", "IdontKnow" },
                    { 4, "Irfa", "Naza" },
                    { 5, "Irfannn", "Naazaaaand" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Professors",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Professors",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Professors",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Professors",
                keyColumn: "Id",
                keyValue: 5);
        }
    }
}
