﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using Task12PostGrad.Models;

namespace Task12PostGrad
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            List<Professor> Professors = new List<Professor>
            {
                new Professor {Id = 1, FirstName = "Nick", LastName = "Lennox"},
                new Professor {Id = 2, FirstName = "Nickboii", LastName = "Lennox"},
                new Professor {Id = 3, FirstName = "Irfan", LastName = "Nazand"},
                new Professor {Id = 4, FirstName = "Dewald", LastName = "Idontknow"},
                new Professor {Id = 5, FirstName = "Svenny", LastName = "Sven"}
            };

            string jsonString;
            jsonString = JsonConvert.SerializeObject(Professors);
            Console.WriteLine(jsonString);
        }
    }
}
